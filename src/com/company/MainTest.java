package com.company;

import com.sun.tools.javac.util.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by stefanstolniceanu on 3/30/17.
 */
class MainTest {
    String example;

    @BeforeEach
    void setUp() {
        this.example = "Example";
    }

    @AfterEach
    void tearDown() {
        System.out.println(this.example);
        this.example = new String();
    }

    @Test
    void main() {
        assertEquals(this.example, "Example");
    }

}